job "${PREFIX}_reposync_${REPOID}" {
  datacenters = ["meyrin"]

  type = "batch"

  spread {
    attribute = "${node.unique.name}"
  }

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  reschedule {
    attempts  = 1
    interval  = "1h"
    delay     = "5m"
    unlimited = false
  }

  task "${PREFIX}_reposync_${REPOID}" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/reposync/reposync:${CI_COMMIT_REF_NAME}"
      force_pull = ${FORCE_PULL}
      logging {
        config {
          tag = "${PREFIX}_reposync"
        }
      }
      volumes = [
        "${MOUNTPOINT}:/repo",
        "/etc/cdn.redhat.com:/certs",
      ]
    }

    env {
      REPOID = "$REPOID"
      REPOFILE = "$REPOFILE"
      REPOPATH = "$REPOPATH"
      CHECKSUM = "$CHECKSUM"
      RUN_REPOSYNC = "$RUN_REPOSYNC"
      RUN_CREATEREPO = "$RUN_CREATEREPO"
    }

    resources {
      cpu = 500 # Mhz
      memory = 512 # MB

      network {
        mbits = 90
      }
    }

  }
}
